.. gsoc.beagleboard.io documentation master file, created by
   sphinx-quickstart on Fri Jan 19 02:18:33 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

:html_theme.sidebar_secondary.remove: true
:sd_hide_title: true

GSoC @ BeagleBoard.org
#######################

.. image:: _static/images/project-with-beagleboard.webp

.. grid:: 1 1 1 3
   :margin: 4 4 0 0
   :gutter: 2

   .. grid-item-card::
      :link: https://openbeagle.org/

      :fab:`gitlab;fa-fade pst-color-dark` Open Source Development
      ^^^^^^^^^^

      Experience working on impactful open source project which
      will be used by thousands of people around the world!

   .. grid-item-card::
      :link: https://www.beagleboard.org/boards

      :fas:`microchip;fa-fade pst-color-dark` Free Hardware
      ^^^^^^^^^^

      Recieve BeagleBoard.org open source single board computer,
      debugging tools, and other required hardware for free!

   .. grid-item-card::
      :link: https://developers.google.com/open-source/gsoc/help/student-stipends

      :fas:`sack-dollar;fa-fade pst-color-dark` Handsome Stipend
      ^^^^^^^^^^

      Earn while making impactful contributions to the open source
      community, Stipend can be upto $6600 based on your country.

.. image:: _static/images/bash-and-boris-gosc-proposal.webp

.. grid:: 1 1 1 3
   :margin: 4 4 0 0
   :gutter: 1

   .. grid-item:: 

      .. button-ref:: gsoc-contributor-guide
         :color: info
         :outline:
         :expand:

   .. grid-item::
      .. button-ref:: gsoc-project-ideas
         :color: info
         :outline:
         :expand:

   .. grid-item::

      .. button-ref:: gsoc-proposal-guide
         :color: info
         :outline:
         :expand:

.. admonition:: Did you know?

   BeagleBoard.org has been accepted to be a mentoring organization in the 
   Google Summer of Code (GSoC) for twelve previous years!


.. admonition:: BeagleBoard.org background
   :class: admonition-clock-back

   BeagleBoard.org is a volunteer organization that seeks to advance the state of
   `open-source software <http://en.wikipedia.org/wiki/Open-source_software>`_ on `open-source
   hardware <https://en.wikipedia.org/wiki/Open-source_hardware>`_ platforms capable of
   running high-level languages and operating systems (like Linux and Zephyr) in embedded
   environments. Born from taking mobile phone processors and putting them on low-cost boards to build affordable
   desktop computers, BeagleBoard.org has evolved to focus on the needs of the "maker" community with greater focus
   on the I/O needed for controlling motors and reading sensors to build things like robots, 3d printers, flying
   drones, in-car computer systems and much more.  BeagleBoard has `inspired creation of the Raspberry Pi <https://web.archive.org/web/20120302232755/www.linuxuser.co.uk/features/raspberry-pi-interview-eban-upton-reveals-all>`_, built over 6 million computers and made PocketBeagle available world-wide for about $25, but is more than a throw-away computer. It is an instance
   of true open hardware, exposing users to the broader world of electronics, demystifying computers and fostering an environment
   of clones that have changed the industry for good.

   Learn more at https://www.beagleboard.org/about.

.. admonition:: Some inspirational past projects
   :class: admonition-youtube hint

   Get inspiration from some :ref:`Past_Projects`.

   .. grid:: 1 1 1 3
      :margin: 4 4 0 0
      :gutter: 4

      .. grid-item::

         .. youtube:: -giV6Xr8RtY
            :width: 100%

      .. grid-item::

         .. youtube:: RWBzyHNetOE
            :width: 100%

      .. grid-item::

         .. youtube:: CDbEAq33vdA
            :width: 100%

.. admonition:: Some other amazing places to find Beagles
   :class: hint

   Beagles have been to the depths of space and the oceans, as well as
   made cheeseburgers. Check the below projects made by incredible
   professional innovators for some inspiration.

   .. grid:: 1 1 2 2
      :margin: 4 4 0 0
      :gutter: 4
      
      .. grid-item-card::

         :fas:`rocket;pst-color-primary` Europa Rover Prototype
         ^^^^^^^^^^

         .. youtube:: sY5WQG3-3mo
            :width: 100%

      .. grid-item-card::

         :fas:`burger;pst-color-primary` Cheeseburger Robot
         ^^^^^^^^^^

         .. youtube:: CbL_3le40qc
            :width: 100%

.. image:: _static/images/explore-ideas.webp
   :align: center
   :target: ideas/index.html

.. toctree::
   :maxdepth: 2
   :hidden:

   ideas/index
   guides/index
   proposals/index
   projects/index
