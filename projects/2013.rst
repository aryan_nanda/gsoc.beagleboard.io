.. _gsoc-2013-projects:

:far:`calendar-days` 2013
###########################

Minix I2C Project for BeagleBoard.org
**************************************

.. youtube:: dxjaFsWYkAM
   :width: 100%

| **Summary:** The project is to develop a generic I2C driver subsystem for Minix, and then use it to write drivers for I2C devices on the new BeagleBone Black.

**Contributor:** Thomas Cort

**Mentors:** Kees Jongenburger, Frans Meulenbroeks, Ben Gras

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2013/orgs/beagle/projects/tcort.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://github.com/tcort/minix-i2c
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub repo

beagle ROS
**********

.. youtube:: JTsZL-puy-E
   :width: 100%

| **Summary:** Integration of the Robot Operative System (ROS) and the BeagleBone.

**Contributor:** Victor Mayoral Vilches

**Mentors:** Koen Kooi 

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2013/orgs/beagle/projects/vmayoral.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://vmayoral.github.io/beagle-ros/
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub pages

BeagleBone Black : Using Userspace-Arduino libraries
*****************************************************

.. youtube:: 1F0S5ajq-Ls
   :width: 100%

| **Summary:** The project proposes to build a set of Arduino-like libraries for development on the BeagleBone Black. The libraries will be available inside of the familiar Arduino IDE(along with multiple communication interfaces) or could also be installed as native libraries on the board. The underlying aim of the project is to make development on embedded Linux platforms easier and familiar to the Arduino environment. 

**Contributor:** Anuj Deshpande, Pranav Nagersheth

**Mentors:** David Anders, Andrew Bradford, Matt Porter, Luis Gustavo Lira

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2013/orgs/beagle/projects/anujdeshpande92.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/Userspace_Arduino
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux page

Booting BeagleBone Black/White via Android
******************************************

.. youtube:: hxzoz6f4Q5w
   :width: 100%

| **Summary:** The goal of this project is to ROM boot an BBB/BBW from an Android device.

**Contributor:** Vlad Victor Ungureanu

**Mentors:** Vladimir Pantelic, Tom King, Kees Jongenburger

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2013/orgs/beagle/projects/ungureanuvladvictor.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://vladu.dev/BBBlfs/
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub pages

Linux ADC IIO Support
**********************

| **Summary:** This project addresses issues in the Beaglebone's IIO ADC driver, rectifying the absence of /sysfs entries and minor discrepancies in /dev/iio:deviceX entries through patchwork. A new hwmon driver for power measurement utilizing the 7th ADC channel is in development. Reports of touch screen driver conflicts with free ADC channels are being investigated and resolved. The resulting improvements will significantly enhance ADC accessibility for newcomers and userspace application programmers on the Beaglebone platform. Additionally, the PWM driver's sysfs entry additions will be addressed through a cleaner implementation of sysfs support for the PWM driver API.

**Contributor:** Zubair Lutfullah Kakakhel

**Mentors:** Greg Kroah-Hartman, Koen Kooi, Vladimir Pantelic, Tom Rini, Laine Walker-Avina

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2013/orgs/beagle/projects/zubairlk.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://github.com/ZubairLK/kernel
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub repo

.. tip:: 

   Checkout eLinux page for GSoC 2013 projects `here <https://elinux.org/BeagleBoard/GSoC/20113_Projects>`_ for more details.
