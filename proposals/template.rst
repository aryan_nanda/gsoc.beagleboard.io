.. _gsoc-proposal-template:

Proposal template - Author
##########################

Introduction
*************

Summary links
=============

- **Contributor:** `Ayush Singh <https://forum.beagleboard.org/u/ayush1325>`_
- **Mentors:** `Jason Kridner <https://forum.beagleboard.org/u/jkridner>`_, `Vaishnav Acath <https://forum.beagleboard.org/u/vaishnav>`_
- **Code:** `Google Summer of Code / greybus / cc1352-firmware · GitLab <https://openbeagle.org/gsoc/greybus/cc1352-firmware>`_
- **Documentation:** `Ayush Singh / docs.beagleboard.io · GitLab <https://openbeagle.org/ayush1325/docs.beagleboard.io>`_
- **GSoC:** `Google Summer of Code <https://summerofcode.withgoogle.com/archive/2023/projects/iTfGBkDk>`_ 

Status
=======

This project is currently just a proposal.

Proposal
========

Please complete the requirements listed on :ref:`gsoc-contributor-guide` 
and fill out this template.

About 
=====

- **Forum:** :fab:`discourse` `u/ayush1325 (Ayush Singh) <https://forum.beagleboard.org/u/ayush1325>`_
- **OpenBeagle:** :fab:`gitlab` `ayush1325 (Ayush Singh) <https://openbeagle.org/ayush1325>`_
- **IRC:** :fas:`comments` `jkridner (Jason Kridner) <https://web.libera.chat/gamja/#beagle>`_
- **Github:** :fab:`github` `jadonk (Jason Kridner) <https://github.com/jadonk>`_
- **School:** :fas:`school` Greatest University
- **Country:** :fas:`flag` Worldistan
- **Primary language:** :fas:`language` Igpay Atinlay
- **Typical work hours:** :fas:`clock` 8AM-5PM US Eastern
- **Previous GSoC participation:** :fab:`google` N/A

Project
********

**Project name:** About my super cool project.

Description
============

In 10-20 sentences, what are you making, for whom, why and with what technologies 
(programming languages, etc.)? (We are looking for open source SOFTWARE submissions. By the way, Verilog for programming an FPGA is considered software by us.)

Software
=========

Which software or technology stack are you going to use to complete this project.

Hardware
========

A list of hardware that you are going to use for this project.

Timeline
********

Provide a development timeline with 10 milestones, one for each week of development without 
an evaluation, and any pre-work. (A realistic, measurable timeline is critical to our selection process.)

.. note:: This timeline is based on the `official GSoC timeline <https://developers.google.com/open-source/gsoc/timeline>`_


Timeline summary
=================

.. table:: 

    +------------------------+----------------------------------------------------------------------------------------------------+
    | Date                   | Activity                                                                                           |                                  
    +========================+====================================================================================================+
    | February 26            | Connect with possible mentors and request review on first draft                                    |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | March 4                | Complete prerequisites, verify value to community and request review on second draft               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | March 11               | Finalized timeline and request review on final draft                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | March 21               | Submit application                                                                                 |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | May 1                  | :ref:`gsoc-template-bonding`                                                                       |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | May 27                 | :ref:`gsoc-template-coding`                                                                        |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 3                 | :ref:`gsoc-template-m1`                                                                            |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 10                | :ref:`gsoc-template-m2`                                                                            |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 17                | :ref:`gsoc-template-m3`                                                                            |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 24                | :ref:`gsoc-template-m4`                                                                            |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 1                 | :ref:`gsoc-template-m5`                                                                            |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 8                 | :ref:`gsoc-template-midterm`                                                                       |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 15                | :ref:`gsoc-template-m6`                                                                            |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 22                | :ref:`gsoc-template-m7`                                                                            |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 29                | :ref:`gsoc-template-m8`                                                                            |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | August 5               | :ref:`gsoc-template-m9`                                                                            |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | August 12              | :ref:`gsoc-template-m10`                                                                           |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | August 19              | :ref:`gsoc-template-final`                                                                         |
    +------------------------+----------------------------------------------------------------------------------------------------+

Timeline detailed
=================

.. _gsoc-template-bonding:

Community Bonding Period (May 1st - May 26th)
----------------------------------------------------------

GSoC contributors get to know mentors, read documentation, get up to speed to begin working on their projects

.. _gsoc-template-coding:

Coding begins (May 27th)
----------------------------------------------------------

.. _gsoc-template-m1:

Milestone #1, Introductory YouTube video (June 3rd)
----------------------------------------------------------

.. _gsoc-template-m2:

Milestone #2 (June 10th)
----------------------------------------------------------

.. _gsoc-template-m3:

Milestone #3 (June 17th)
----------------------------------------------------------

.. _gsoc-template-m4:

Milestone #4 (June 24th)
----------------------------------------------------------

.. _gsoc-template-m5:

Milestone #5 (July 1st)
----------------------------------------------------------

.. _gsoc-template-midterm:

Submit midterm evaluations (July 8th)
----------------------------------------------------------

.. important:: 
    
    **July 12 - 18:00 UTC:** Midterm evaluation deadline (standard coding period) 

.. _gsoc-template-m6:

Milestone #6 (July 15th)
----------------------------------------------------------

.. _gsoc-template-m7:

Milestone #7 (July 22nd)
----------------------------------------------------------

.. _gsoc-template-m8:

Milestone #8 (July 29th)
----------------------------------------------------------

.. _gsoc-template-m9:

Milestone #9 (Aug 5th)
----------------------------------------------------------

.. _gsoc-template-m10:

Milestone #10 (Aug 12th)
----------------------------------------------------------

.. _gsoc-template-final:

Final YouTube video and work upload to GSoC site (Aug 19th)
-----------------------------------------------------------

Submit final project video, submit final work to GSoC site 
and complete final mentor evaluation

Final Submission (Aug 24nd)
-----------------------------

.. important::

    **August 19 - 26 - 18:00 UTC:** Final week: GSoC contributors submit their final work 
    product and their final mentor evaluation (standard coding period)

    **August 26 - September 2 - 18:00 UTC:** Mentors submit final GSoC contributor 
    evaluations (standard coding period)

Initial results (September 3)
-----------------------------

.. important:: 
    **September 3 - November 4:** GSoC contributors with extended timelines continue coding

    **November 4 - 18:00 UTC:** Final date for all GSoC contributors to submit their final work product and final evaluation

    **November 11 - 18:00 UTC:** Final date for mentors to submit evaluations for GSoC contributor projects with extended deadline

Experience and approach
***********************

In 5-15 sentences, convince us you will be able to successfully complete your project in the timeline you have described.

Contingency
===========

What will you do if you get stuck on your project and your mentor isn’t around?

Benefit
========

If successfully completed, what will its impact be on the `BeagleBoard.org <https://www.beagleboard.org/>`_ community? Include quotes from `BeagleBoard.org <https://www.beagleboard.org/>`_.
community members who can be found on our `Discord <https://bbb.io/gsocchat>`_ and `BeagleBoard.org forum <https://bbb.io/gsocml/13>`_.

Misc
====

Please complete the requirements listed in the `General Requirements <https://forum.beagleboard.org/t/gsoc-ideas/35850#general-requirements-5>`_. 
Provide link to merge request.

Suggestions
===========

Is there anything else we should have asked you?
