.. _RISC-V_CPU_core_in_FPGA_fabric:

Low-latency I/O RISC-V CPU core in FPGA fabric - Atharva Kashalkar
##################################################################

Introduction
*************

Implementation of PRU subsystem on BeagleV-Fire's FPGA fabric, resulting in a real-time microcontroller system working alongside the main CPU, providing low-latency access to I/O .

Summary links
=============

- **Contributor:** `Atharva Kashalkar <https://forum.beagleboard.org/u/roger18>`_
- **Mentors:** `Jason Kridner <https://forum.beagleboard.org/u/jkridner>`_, `Cyril Jean <https://forum.beagleboard.org/u/vauban>`_
- **Code Repository:**
- - *Upstream Repository:* `BeagleV-Fire gateware <https://openbeagle.org/beaglev-fire/gateware>`_
- - *Daily Code Check-in Repository:* `Fork for BeagleV-Fire gateware <https://openbeagle.org/Roger18/gateware>`_
- **Weekly/biweekly Updates Forums Thread:** `Progress Reports <https://forum.beagleboard.org/t/weekly-progress-report-low-latency-i-o-risc-v-cpu-core-in-fpga-fabric/38488>`_
- **Blog:** `TechTales <https://rapidroger18.github.io/>`_

Status
=======

This project is selected for GSOC 2024.

Proposal
========

| Accounts created across `OpenBeagle <https://openbeagle.org/Roger18>`_, `Discord <https://discordapp.com/users/539789356228018200>`_ and `Beagle Forum <https://forum.beagleboard.org/u/roger18>`_
| PR was created for the cross-compilation task.

About 
=====

- **Resume:** Find my resume `here <https://docs.google.com/document/d/1NK4dAuWjMRkkG4YJrMSHrUTxJs0s7_JUR-DipmuclWQ/edit?usp=sharing>`_
- **Forum:** :fab:`discourse` `u/roger18 (Atharva Kashalkar) <https://forum.beagleboard.org/u/roger18>`_
- **OpenBeagle:** :fab:`gitlab` `Roger18 (Atharva Kashalkar) <https://openbeagle.org/Roger18>`_
- **Github:** :fab:`github` `RapidRoger18 (Atharva Kashalkar) <https://github.com/RapidRoger18>`_
- **School:** :fas:`school` Veermata Jijabai Technological Institute (VJTI)
- **Country:** :fas:`flag` India
- **Primary language:** :fas:`language` English Hindi
- **Typical work hours:** :fas:`clock` 9 AM-11 PM Indian Standard Time
- **Previous GSoC participation:** :fab:`google` First Time Applicant

Project
********

**Project name:** Low-latency I/O RISC-V CPU core in FPGA fabric.

Description
============
To provide the capability of a Programmable Real-time Unit Industrial Control SubSystem (PRU-ICSS), which is present on several BeagleBone boards, I propose to deploy an existing RISC-V 32IM core with a customized Instruction Set on FPGA Fabric present on BeagleV-Fire. The goal of this deployment is to provide high bandwidth between the CPU and I/O, resulting in a on-board microcontroller.

Goals and Objectives
====================

The main aim of this project is to deploy a soft core subsystem on BeagleV-Fire’s FPGA fabric, functionally equivalent to PRU subsystem on BeagleBone Black. The core will feature RISC-V ISA customised to perform ultra low-latency I/O operations, i.e., single-cycle execution. This deployment will provide high-bandwidth data transfer in main CPU and I/O and also ensure high speed data processing similar to a microcontroller.

The programmable nature of the PRU, along with its access to pins, events, and all SoC resources, provides flexibility in implementing fast real-time responses, specialised data handling operations, custom peripheral interfaces, and in offloading tasks from the main processor cores of the system-on-chip (SoC).

.. image:: ../_static/images/PRU_diagram.png 

Based on the requirements of the project, it is most efficient to use PicoRV (an open source RISC-V based processor) as a base and modify it to perform high speed I/O operations. The PicoRV possesses excellent compiler support and diverse instruction set but lacks I/O support and single-cycle execution for some instructions.

This problem will be resolved within the first stage of the project, which will focus on making the core I/O compatible and modifying its execution flow to ensure single-cycle execution for all instructions. Some present soft processor IPs like AMD’s Microblaze used in Vivado Design Suite and Microchip’s Mi-V used in Libero Design suite can provide good insights on how a functioning soft core IP will look like.
As the Stage-1 of this project concludes with deployment of the RV core, Stage-2 will focus on establishing a communication medium between the PRU and the main CPU. This will ensure the ‘on-the-fly’ programming for the PRU and high bandwidth data transfer from I/O to the main CPU.

*riscv64-unknown-elf-gcc* compiler will compile the C program into bare-metal RISC-V based binary instructions within the linux booted on the main CPU. The communication between main CPU and PRU will be used to send these instructions into the program memory of PRU without needing to flash the FPGA each time.

This connection can be established in multiple ways: 

1. The Program Memory of the PRU can be written into SPI flash that contains FPGA logic, and the data transfer will take place through 32-bit interconnecting AXI bus.
2. Shared memory between PRU and Main CPU can be used of PRU memory and Data Transfer.
3. 32-bit interconnecting AXI bus can be used to write instructions into Program memory of PRU which will be printed on FPGA logic, the same AXI bus will be used for other Data Transfers.

The 32-bit interconnecting AXI bus is more suitable for burst write data to RAMs, thus using shared memory or SPI flash makes suitable method to serve as a communication medium between PRU and CPU to ensure efficient and smooth processing.

At the conclusion of the project, BeagleV-Fire will host a fully functional PRU system that can be controlled and programmed through the main CPU.

Software
=========

 - Verilog HDL.
 - Verilator.
 - Libero SoC suite.
 - Microchip Softconsole
 - ModelSim ME.
 - Linux.
 - OpenBeagle CI.
 
Hardware
========

Ability to program BeagleV-Fire using serial port and set up JTAG for effective debugging. 

Timeline
********

Timeline summary
=================

.. table:: 

    +------------------------+----------------------------------------------------------------------------------------------------+
    | Date                   | Activity                                                                                           |                                  
    +========================+====================================================================================================+
    | April                  | Understand detailed use cases of existing cores and shortlist them based on requirements           |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | May 1                  | Start bonding - Discussing implementation methods with Mentors                                     |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | May 15                 | College Examinations                                                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 1                 | Start coding and introductory video                                                                |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 3                 | :ref:`RVFMilestone1`                                                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 10                | :ref:`RVFMilestone2`                                                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 17                | :ref:`RVFMilestone3`                                                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 24                | :ref:`RVFMilestone4`                                                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 1                 | :ref:`RVFMilestone5`                                                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 8                 | Submit midterm evaluations                                                                         |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 15                | :ref:`RVFMilestone6`                                                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 22                | :ref:`RVFMilestone7`                                                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 29                | :ref:`RVFMilestone8`                                                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | August 5               | :ref:`RVFMilestone9`                                                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | August 12              | :ref:`RVFMilestone10`                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | August 19              | Submit final project video, submit final work to GSoC site, and complete final mentor evaluation   |
    +------------------------+----------------------------------------------------------------------------------------------------+

Timeline detailed
=================

Community Bonding Period (May 1st - May 15th)
-------------------------------------------------------

 - Get to know mentors and discuss project implementation.
 - read documentation, and get up to speed to begin working on the projects
 - shortlisting pre-existing cores based on initial assessment, by reading available documentation. 

Coding begins (May 27th)
-------------------------------------------------------

.. _RVFMilestone1:

Milestone #1, Introductory YouTube video (June 3rd)
-----------------------------------------------------
 - Make an introductory video
 - Researching through other soft core IPs to understand the I/O interfacing used in the core.
 - Setting up remote access on BeagleV-Fire and completing the LED-blink tutorial given in the documentation.

.. _RVFMilestone2:

Milestone #2, Modifiying RV core of I/O(June 10th)
-------------------------------------------------------
 - Modification of the PicoRV core to interface GPIOs and memory.
 - Removing unnecessary extensions to reduce size and complexity, without changing its efficiency.

.. _RVFMilestone3:

Milestone #3, Modifying RV core for single-cycle execution(June 17th)
-------------------------------------------------------------------------
 - Modification of the PicoRV core for single-cycle execution of all instructions.
 - This is to make sure the PRU functions as required after modifications.  

.. _RVFMilestone4:

Milestone #4, BeagleV-Fire setup (June 24th)
-------------------------------------------------------
 - Integration of PRU core with BeagleV-Fire gateware to ensure PRU deployment through gateware.
 - Temporary setup the program memory on FPGA logic for testing and verification of the core.

.. _RVFMilestone5:

Milestone #5, Verification of the core (July 1st)
-------------------------------------------------------
 - Verification of all modifications to ensure correct instruction execution. This will be done using predefined verification methods.
 - Ensuring stable PRU workflow.

Submit midterm evaluations (July 8th)
-------------------------------------------------------
 - Complete pending Stage 1 tasks, if any.

.. important:: 
    
    **July 12 - 18:00 UTC:** Midterm evaluation deadline (standard coding period) 

.. _RVFMilestone6:

Milestone #6, Establishing communication (July 15th)
-------------------------------------------------------
 - Setup the program memory of the PRU within SPI flash as FPGA.
 - Setting AXI bus access for data transfer.

.. _RVFMilestone7:

Milestone #7, Establishing communication (July 22nd)
-------------------------------------------------------
 - Using Shared memory for data transfer and comparing the results with using AXI bus and selecting faster and reliable option.

.. _RVFMilestone8:

Milestone #8, Setup Access (July 29th)
-------------------------------------------------------
 - Setting up a easy build script to compile the C program and send it to PRU's Program Memory.
 - Necessary changes to TCL scripts and Device Tree. 

.. _RVFMilestone9:

Milestone #9, Testing and Verification(Aug 5th)
-------------------------------------------------------
 - Setting up gitlab CI/CD to include necessary files in the archives.
 - Testing smooth and easy workflow from C program to execution in PRU.
 - Testing the GPIO bandwidth and CPU resources with and without the use of PRU. 

.. _RVFMilestone10:

Milestone #10, Documentation and Tutorial(Aug 12th)
-------------------------------------------------------
 - Documenting the project and ways to access PRU on docs.beagleboard.org.
 - Having an LED Blink tutorial for users to familiarize themselves with the PRU.

Final YouTube video (Aug 19th)
-------------------------------------------------------

Submit final project video, submit final work to GSoC site, and complete final mentor evaluation

Final Submission (Aug 24th)
-------------------------------------------------------

.. important::

    **August 19 - 26 - 18:00 UTC:** Final week: GSoC contributors submit their final work 
    product and their final mentor evaluation (standard coding period)

    **August 26 - September 2 - 18:00 UTC:** Mentors submit final GSoC contributor 
    evaluations (standard coding period)

Initial results (September 3)
-------------------------------------------------------

.. important:: 
    **September 3 - November 4:** GSoC contributors with extended timelines continue coding

    **November 4 - 18:00 UTC:** Final date for all GSoC contributors to submit their final work product and final evaluation

    **November 11 - 18:00 UTC:** Final date for mentors to submit evaluations for GSoC contributor projects with an extended deadline

Experience
***********

This project will require prior knowledge of Risc-V ISA, FPGA programming, Assembly, and Verilog.

| I have previously worked on a project `Risc-V 32IM core <https://github.com/NachtSpyder04/RISC-V-Eklavya-23>`_ where I contributed towards the synthesis of a Risc-V core using Vivado Design suite and flashed it on UPduino FPGA using Yosys framework. This CPU was successfully able to run operations like the Fibonacci series and factorial of a number.
| I was a Team Leader of the Semi-finalist Team in `E-Yantra Robotics Competition (eYRC) <https://portal.e-yantra.org/>`_, IIT Bombay on Theme "AstroTinker Bot", where we had to develop a single cycle Risc-V 32I core that was capable of implementing Dijktra Algorithm written in C code and cross-compiled to Risc-V binary instructions. This Competition exposed me to various debugging methods using JTAG connection through Quartus Prime Lite software. DE0-NANO development board with Cyclone IV FPGA was used to control the bot. The bot used PID-based Line Following and Electromagnet to pick and place blocks through an arena representing a Space Station. Verilog HDL codes for the project can be found on `E-yantra_Astrotinker-bot github repo. <https://github.com/RapidRoger18/E-yantra_Astrotinker-bot>`_
| I have prior experience with ESP32 microcontrollers for small projects.

Approach
*********
| As a starting point, I have forked the BeagleV-Fire gateware Repository and completed the `Blinky Tutorial <https://openbeagle.org/Roger18/gateware/-/tree/main/sources/FPGA-design/script_support/components/CAPE/BLINKY_TRY>`_ as specified on `BeagleBoard Docs <https://docs.beagleboard.org/latest/boards/beaglev/fire/demos-and-tutorials/gateware/customize-cape-gateware-verilog.html>`_ 
| Continuing on this I have set up Libero SoC suite and Softconsole on my Local machine that will provide an easy debugging method using JTAG. I have tested this setup by implementing a simple UART receiver and simulating it in ModelSim ME using a simple testbench.

| The concept of having a microprocessor and a microcontroller on a single SoC is amazing. I can see a variety of applications where a SoC like BeagleV-Fire would be ideal. This inspires me to take on this project and give my contribution to the open-source community. I have my full commitment to this project and am willing to put in complete efforts to complete the project within the allocated time.
| I would also like to keep improving with the project after GSoC.

Contingency
===========

If I get stuck on my project and my mentor isn’t around, I will use the following resources:-
 - `BeagleV-Fire <https://docs.beagleboard.io/latest/boards/beaglev/fire/index.html>`_
 - `PRU-Documentation <https://inst.eecs.berkeley.edu/~ee192/sp20/files/am335x-pru.pdf>`_
 - `Picorv32 Docs <https://github.com/YosysHQ/picorv32/blob/master/README.md>`_ 
 - `PRU cookbook <https://docs.beagleboard.org/books/pru-cookbook/index.html>`_
 - `TI PRU documentation <https://software-dl.ti.com/processor-sdk-linux/esd/AM62X/09_00_00_03/exports/docs/linux/Foundational_Components_PRU_Subsystem.html>`_
 
Moreover, the BeagleBoard community is very helpful in resolving doubts, I will use OpenBeagle forums to clarify any doubts left after referring to the above resources.

Benefit
========
This project will not only improve the use cases of BeagleV-Fire but also provide a very fast real-time subsystem that can be used as a microcontroller. It will provide the following functionalities: -
 - Reduce memory and I/O resource consumption on the main CPU by performing basic computations in PRU itself thus providing more resources to perform complex tasks.
 - Provide a real-time interface for fast, deterministic operations.
 - Reprogammability of FPGA will allow to deployment of PRU only when necessary, thus providing additional logical elements when PRU is not being used.
 - The configurable nature of FPGA will allow multiple levels of customizations and configurations on PRU, enabling the user to efficiently meet their requirements in the lowest possible resources.   

The success of this project will result in an all-in-one SoC meeting all the requirements of `BeagleBoard.org <https://www.beagleboard.org/>`_ community.

Misc
====

PR request for cross-compilation task `#182 <https://github.com/jadonk/gsoc-application/pull/182>`_ 
